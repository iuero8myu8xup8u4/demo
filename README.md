# Demo Lesson!

```
         _nnnn_        
        dGGGGMMb       
       @p~qp~~qMb      
       M|@||@) M|      
       @,----.JM|      
      JS^\__/  qKL     
     dZP        qKRb   
    dZP          qKKb  
   fZP            SMMb 
   HZM            MMMM 
   FqM            MMMM 
 __| ".        |\dS"qML
 |    `.       | `' \Zq
_)      \.___.,|     .'
\____   )MMMMMP|   .'  
     `-'       `--' hjm
     
```

## Getting started


* Launch this notebook: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/iuero8myu8xup8u4/demo/master?labpath=notebook%2Fdemo.ipynb)

* Go to the projects folder and get coding!
